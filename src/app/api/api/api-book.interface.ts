export interface Book {
  id: string;
  title: string;
  description: string;
  thumbnailUrl: string;
}

export interface SearchedBooks {
  books: Book[];
  totalItems: number;
}
