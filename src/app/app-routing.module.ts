import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {WhitelistPageComponent} from './pages/whitelist-page/whitelist-page.component';
import {AuthGuard} from './api/auth/auth.guard';



const routes: Routes = [{
  path: 'login',
  component: LoginPageComponent
}, {
  path: '',
  component: HomePageComponent,
  canActivate: [AuthGuard]
}, {
  path: 'whitelist',
  component: WhitelistPageComponent,
  canActivate: [AuthGuard]
}, {
  path: '**',
  redirectTo: ''
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
