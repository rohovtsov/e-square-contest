import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AuthService} from '../../api/auth/auth.service';
import {BooksPopupService} from '../../api/books/books-popup.service';



@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit {

  constructor(
    public auth: AuthService,
    public popup: BooksPopupService
  ) { }

  ngOnInit(): void {
  }

}
