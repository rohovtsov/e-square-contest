import {Injectable} from '@angular/core';
import {Book} from '../api/api-book.interface';
import {Subject} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class BooksPopupService {
  private booksSubject = new Subject<Book>();
  public books$ = this.booksSubject.asObservable();

  constructor() { }

  openPopup(book: Book): void {
    this.booksSubject.next(book);
  }
}
