import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {LoginFormData} from '../../components/login-form/login-form.interface';
import {AuthService} from '../../api/auth/auth.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPageComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.auth.isAuth) {
      this.navigateHome();
    }
  }

  onSubmit(data: LoginFormData) {
    this.auth.authorizeUser(data.username);
    this.navigateHome();
  }

  private navigateHome(): void {
    this.router.navigateByUrl('/');
  }
}
