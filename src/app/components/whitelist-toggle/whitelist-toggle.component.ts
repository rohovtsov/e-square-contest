import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {Book} from '../../api/api/api-book.interface';
import {BooksWhitelistService} from '../../api/books/books-whitelist.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';



@Component({
  selector: 'app-whitelist-toggle',
  templateUrl: './whitelist-toggle.component.html',
  styleUrls: ['./whitelist-toggle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WhitelistToggleComponent implements OnInit {
  @Input() book: Book;
  whitelist$: Observable<WhitelistData>;

  constructor(
    public whitelist: BooksWhitelistService
  ) { }

  ngOnInit(): void {
    this.whitelist$ = this.whitelist.books$.pipe(
      map(() => ({
        isWhitelisted: this.whitelist.isWhitelisted(this.book)
      }))
    );
  }
}

interface WhitelistData {
  isWhitelisted: boolean;
}
