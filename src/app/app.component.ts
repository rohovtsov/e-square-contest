import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  @HostBinding('class.is-loaded') isLoaded = false;

  constructor() { }

  ngOnInit(): void {
  }

  onRouteActivate(): void {
    setTimeout(() => {
      this.isLoaded = true;
    });
  }

  onRouteDeactivate(): void {
    this.isLoaded = false;
  }
}
