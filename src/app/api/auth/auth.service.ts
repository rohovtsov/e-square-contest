import { Injectable } from '@angular/core';
import {UserData} from './user.interface';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: UserData = null;

  constructor() { }

  get isAuth(): boolean {
    return !!this.userData;
  }

  authorizeUser(username: string): void {
    this.userData = { username };
  }
}
