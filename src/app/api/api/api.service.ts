import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Book, SearchedBooks} from './api-book.interface';
import {catchError, map} from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  public searchBooks(query: string, maxResults: number = 20, startIndex: number = 0): Observable<SearchedBooks> {
    return this.http.get(environment.apiUrl + '/books/v1/volumes', {
      params: {
        q: query,
        startIndex: startIndex.toString(),
        maxResults: maxResults.toString()
      }
    }).pipe(
      map((data: any): SearchedBooks => {
        return {
          totalItems: data.totalItems,
          books: (data.items ?? []).map((item: any) => {
            const imageThumbnails = item?.volumeInfo?.imageLinks ?? {};
            const thumbnailUrl = imageThumbnails?.thumbnail ?? imageThumbnails?.smallThumbnail;

            return {
              id: item?.id,
              title: item?.volumeInfo?.title ?? null,
              description: item?.volumeInfo?.description ?? null,
              thumbnailUrl: thumbnailUrl ?? null
            };
          })
        };
      }),
      catchError(() => of({
        totalItems: 0,
        books: []
      }))
    );
  }
}
