import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { WhitelistPageComponent } from './pages/whitelist-page/whitelist-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { BooksSearcherComponent } from './components/books-searcher/books-searcher.component';
import {HttpClientModule} from '@angular/common/http';
import { BooksGridComponent } from './components/books-grid/books-grid.component';
import { InfoMessageComponent } from './components/info-message/info-message.component';
import {LoaderComponent} from './components/loader/loader.component';
import { BookItemComponent } from './components/book-item/book-item.component';
import { BookPopupComponent } from './components/book-popup/book-popup.component';
import {PageTitleComponent} from './components/page-title/page-title.component';
import { WhitelistToggleComponent } from './components/whitelist-toggle/whitelist-toggle.component';



@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginPageComponent,
    WhitelistPageComponent,
    LoginFormComponent,
    BooksSearcherComponent,
    BooksGridComponent,
    InfoMessageComponent,
    LoaderComponent,
    BookItemComponent,
    BookPopupComponent,
    PageTitleComponent,
    WhitelistToggleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
