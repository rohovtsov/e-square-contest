import {AbstractControl, ValidationErrors} from '@angular/forms';



export const usernameValidator = (control: AbstractControl): ValidationErrors | null => {
  const parsedValue = control.value.trim();

  if (parsedValue === '') {
    return { incorrect: true };
  }

  return null;
};
