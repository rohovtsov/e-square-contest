import {Injectable} from '@angular/core';
import {ApiService} from '../../api/api/api.service';
import {concatMap, debounceTime, distinctUntilChanged, map, shareReplay, startWith, switchMap} from 'rxjs/operators';
import {merge, Observable, of} from 'rxjs';
import {BooksSearchResult} from './books-searcher.interface';
import {Book} from '../../api/api/api-book.interface';



@Injectable()
export class BooksSearcherService {
  public pageLimit = 20;

  constructor(
    public api: ApiService
  ) { }

  public createResults(
    searchQuery$: Observable<string>,
    nextPage$: Observable<never>,
    defaultQuery: string|null
  ): Observable<BooksSearchResult> {
    return searchQuery$.pipe(
      map(query => query.trim()),
      debounceTime(750),
      distinctUntilChanged(),
      startWith(defaultQuery ?? ''),
      this.createSearchQuery(),
      this.createPaginationQueries(nextPage$),
      this.markIfAllPagesLoaded(),
      shareReplay(1)
    );
  }

  private createSearchQuery() {
    return (source: Observable<string>): Observable<BooksSearchResult> => {
      return source.pipe(
        switchMap((query: string) => {
          if (!query) {
            return of({ books: [], isInitial: true, totalItems: 0 });
          }

          return merge(
            of({ isLoading: true, books: [], totalItems: 0 }),
            this.api.searchBooks(query, this.pageLimit).pipe(
              map(res => ({ books: res.books, totalItems: res.totalItems, query }))
            )
          );
        })
      );
    };
  }

  private createPaginationQueries(nextPage$: Observable<never>) {
    return (source: Observable<BooksSearchResult>): Observable<BooksSearchResult> => {
      return source.pipe(
        switchMap((result: BooksSearchResult) => {
          if (result.isLoading) {
            return of(result);
          }

          const allBooks: Book[] = [...result.books];
          let pageId = 0;

          return merge(
            of(result),
            nextPage$.pipe(
              concatMap(() => {
                pageId++;

                return merge(
                  of({ ...result, books: allBooks, isPageLoading: true }),
                  this.api.searchBooks(result.query, this.pageLimit, (this.pageLimit * pageId)).pipe(
                    map(res => {
                      allBooks.push(...res.books);
                      return { ...result, books: [...allBooks], totalItems: res.totalItems };
                    }),
                  )
                );
              })
            )
          );
        })
      );
    };
  }

  private markIfAllPagesLoaded() {
    return (source: Observable<BooksSearchResult>): Observable<BooksSearchResult> => {
      return source.pipe(
        map(results => {
          return {
            ...results,
            isAllPagesLoaded: results.books.length >= results.totalItems
          };
        })
      );
    };
  }
}

