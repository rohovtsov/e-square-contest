import {Injectable} from '@angular/core';
import {Book} from '../api/api-book.interface';
import {BehaviorSubject} from 'rxjs';



const WHITELIST_STORAGE_KEY = 'whitelist_books';

@Injectable({
  providedIn: 'root'
})
export class BooksWhitelistService {
  public books$ = new BehaviorSubject<Book[]>(this.loadBooks());

  constructor() { }

  public addBook(book: Book): void {
    this.writeBooks([
      ...this.books$.value,
      book
    ]);
  }

  public removeBook(book: Book): void {
    const index = this.books$.value.findIndex((book0) => book0.id === book.id);

    if (index < 0) {
      return;
    }

    const newBooks = [...this.books$.value];
    newBooks.splice(index, 1);
    this.writeBooks(newBooks);
  }

  public isWhitelisted(book: Book) {
    if (!book) {
      return false;
    }

    return !!this.books$.value.find((book0) => book0.id === book.id);
  }

  private writeBooks(books: Book[]): void {
    this.books$.next(books);
    this.saveBooks(books);
  }

  private saveBooks(books: Book[]): void {
    if (!localStorage) {
      return;
    }

    localStorage.setItem(WHITELIST_STORAGE_KEY, JSON.stringify(books));
  }

  private loadBooks(): Book[] {
    if (!localStorage) {
      return [];
    }

    return JSON.parse(localStorage.getItem(WHITELIST_STORAGE_KEY) ?? '[]') as Book[];
  }
}
