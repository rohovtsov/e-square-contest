import {Component, OnInit, ChangeDetectionStrategy, Input, HostBinding} from '@angular/core';
import {Book} from '../../api/api/api-book.interface';



@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookItemComponent implements OnInit {
  @Input() @HostBinding('class.is-clickable') isClickable = true;
  @Input() book: Book;
  @Input() hasTitle = true;

  constructor() { }

  ngOnInit(): void {
  }

}
