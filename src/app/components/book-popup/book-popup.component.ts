import {Component, OnInit, ChangeDetectionStrategy, HostBinding, Inject, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {BooksPopupService} from '../../api/books/books-popup.service';
import {map} from 'rxjs/operators';
import {Book} from '../../api/api/api-book.interface';
import {DOCUMENT} from '@angular/common';
import {Subscription} from 'rxjs';



@Component({
  selector: 'app-book-popup',
  templateUrl: './book-popup.component.html',
  styleUrls: ['./book-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookPopupComponent implements OnInit, OnDestroy {
  @HostBinding('class.is-open') get isOpen() {
    return !!this.book;
  }
  book: Book|null = null;
  subscription: Subscription;

  constructor(
    private popups: BooksPopupService,
    private changeDetectorRef: ChangeDetectorRef,
    @Inject(DOCUMENT) private document
  ) { }

  ngOnInit(): void {
    this.subscription = this.popups.books$.pipe(
      map((book) => this.togglePopup(book))
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onClose(): void {
    this.togglePopup(null);
  }

  private togglePopup(book: Book|null): void {
    this.book = book;
    this.document.body.classList.toggle('scrollbar-is-locked', this.isOpen);
    this.changeDetectorRef.detectChanges();
  }
}
