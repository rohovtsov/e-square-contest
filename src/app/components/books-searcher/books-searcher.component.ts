import {Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input} from '@angular/core';
import {ApiService} from '../../api/api/api.service';
import {FormControl} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {BooksSearchResult} from './books-searcher.interface';
import {Book} from '../../api/api/api-book.interface';
import {BooksSearcherService} from './books-searcher.service';



@Component({
  selector: 'app-books-searcher',
  templateUrl: './books-searcher.component.html',
  styleUrls: ['./books-searcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    BooksSearcherService
  ]
})
export class BooksSearcherComponent implements OnInit {
  @Output() bookClick = new EventEmitter<Book>();
  @Input() defaultQuery: string = null;
  public searchControl = new FormControl('');
  public nextPage$ = new Subject<never>();
  public results$: Observable<BooksSearchResult>;

  constructor(
    public searcher: BooksSearcherService,
    public api: ApiService
  ) { }

  ngOnInit(): void {
    if (this.defaultQuery) {
      this.searchControl.setValue(this.defaultQuery);
    }

    this.results$ = this.searcher.createResults(
      this.searchControl.valueChanges,
      this.nextPage$,
      this.defaultQuery
    );
  }
}

