import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BooksWhitelistService} from '../../api/books/books-whitelist.service';
import {BooksPopupService} from '../../api/books/books-popup.service';



@Component({
  selector: 'app-whitelist-page',
  templateUrl: './whitelist-page.component.html',
  styleUrls: ['./whitelist-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WhitelistPageComponent implements OnInit {

  constructor(
    public whitelist: BooksWhitelistService,
    public popup: BooksPopupService
  ) { }

  ngOnInit(): void {
  }

}
