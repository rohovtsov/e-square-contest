import {Book} from '../../api/api/api-book.interface';



export interface BooksSearchResult {
  books: Book[];
  isLoading?: boolean;
  query?: string;
  totalItems?: number;
  isInitial?: boolean;
  isPageLoading?: boolean;
  isAllPagesLoaded?: boolean;
}
