import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginFormData} from './login-form.interface';
import {usernameValidator} from './login-form-validator';



@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {
  @Output() loginSubmit = new EventEmitter<LoginFormData>();
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      username: ['', [Validators.required, usernameValidator]]
    });
  }

  onSubmit(): void {
    this.loginSubmit.emit({
      username: this.formGroup.value.username
    });
  }
}
