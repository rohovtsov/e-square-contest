import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';
import {Book} from '../../api/api/api-book.interface';



@Component({
  selector: 'app-books-grid',
  templateUrl: './books-grid.component.html',
  styleUrls: ['./books-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooksGridComponent implements OnInit {
  @Input() books: Book[] = [];
  @Output() bookClick = new EventEmitter<Book>();

  constructor() { }

  ngOnInit(): void {
  }

  trackByBook(index: number, book: Book): string {
    return book.id || index.toString();
  }
}
